import { StorageKey } from '@/constants';

export function getAuthUser() {
  try {
    return JSON.parse(localStorage.getItem(StorageKey.authUser));
  } catch (error) {
    console.error(error);
    return null;
  }
}

export function setAuthUser(user) {
  localStorage.setItem(StorageKey.authUser, JSON.stringify(user));
}

export function removeAuthUser() {
  localStorage.removeItem(StorageKey.authUser);
}

export function getAuthToken() {
  try {
    return JSON.parse(localStorage.getItem(StorageKey.authToken));
  } catch (error) {
    console.error(error);
    return null;
  }
}

export function setAuthToken(token) {
  localStorage.setItem(StorageKey.authToken, JSON.stringify(token));
}

export function removeAuthToken() {
  localStorage.removeItem(StorageKey.authToken);
}
