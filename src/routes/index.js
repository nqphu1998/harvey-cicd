import React from 'react';
import { Switch } from 'react-router-dom';
import AuthRoute from '@/routes/AuthRoute';
import GuestRoute from '@/routes/GuestRoute';
import HomePage from '@/pages/HomePage';
import LoginPage from '@/pages/LoginPage';
import ProfilePage from '@/pages/ProfilePage';
import RecruitmentPage from '@/pages/RecruitmentPage';
import NotFoundPage from '@/pages/NotFoundPage';
import Seeker from '@/pages/Seeker';
import Provider from '@/pages/Provider';
// eslint-disable-next-line max-len
import CreateApplication from '@/pages/RecruitmentPage/components/Application/components/CreateApplication';
// eslint-disable-next-line max-len
import CreateCandidate from '@/pages/RecruitmentPage/components/Candidate/components/CreateCandidate';

function Routes() {
  return (
    <Switch>
      <AuthRoute exact path="/" component={HomePage} />
      <GuestRoute path="/login" component={LoginPage} />
      <AuthRoute path="/me" component={ProfilePage} />
      <AuthRoute path="/recruitment/:view" component={RecruitmentPage} />
      <AuthRoute path="/create-new-application" component={CreateApplication} />
      <AuthRoute path="/create-new-candidate" component={CreateCandidate} />
      <AuthRoute path="/seeker" component={Seeker} />
      <AuthRoute path="/provider" component={Provider} />
      <AuthRoute path="*" component={NotFoundPage} />
    </Switch>
  );
}

export default Routes;
