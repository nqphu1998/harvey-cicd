import React from 'react';

import Routes from '@/routes';
import { BrowserRouter as Router } from 'react-router-dom';

import '@/assets/scss/global.scss';


function App() {
  return (
      <Router>
        <Routes />
      </Router>
  );
}

export default App;
