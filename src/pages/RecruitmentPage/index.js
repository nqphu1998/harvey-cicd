import React from 'react';

import { useParams } from 'react-router-dom';

import BreadcrumbCustom from '@/components/Breadcrumb';

import { RECRUITMENT_VIEW } from '@/constants';

import ApplicationView from './components/Application/components/ApplicationView';
import CandidateView from './components/Candidate/components/CandidateView';

import './styles.scss';

function Recruitment() {
  const { view } = useParams();

  const data = [{ item: view, to: view }];

  const handleRecruitmentView = () => {
    switch (view) {
      case RECRUITMENT_VIEW.APPLICANT:
        return <ApplicationView />;
      case RECRUITMENT_VIEW.CANDIDATE:
        return <CandidateView />;
      default:
        return <ApplicationView />;
    }
  };

  return (
    <div className="recruitment-container">
      <div className="breadcrumb-wrap">
        <BreadcrumbCustom data={data} mainItem="Recruitment" />
      </div>
      {handleRecruitmentView()}
    </div>
  );
}
export default Recruitment;
