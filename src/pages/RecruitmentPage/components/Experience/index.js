import React from 'react';

import { Menu, Divider } from 'antd';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import PrimaryDropdown from '@/components/PrimaryDropdown';
import PrimaryDatePicker from '@/components/PrimaryDatePicker';
import PrimaryButton from '@/components/PrimaryButton';
import PrimaryTextarea from '@/components/PrimaryTextarea';

import './styles.scss';

function Experience({ buttonContent, className, to }) {
  const menuItems = [
    { item: 'Not enough information' },
    { item: 'Contacted' },
    { item: 'Successful - Auto email' },
    { item: 'Successful - Personal' },
    { item: 'Reject - Permenant' },
    { item: 'Reject - No roles' },
  ];

  const menu = (
    <Menu>
      {menuItems.map((item) => (
        <Menu.Item key={item.item}>{item.item}</Menu.Item>
      ))}
    </Menu>
  );

  function renderExperienceHistory() {
    return (
      <div className="experience-history-container">
        <div className="close-icon">&#215;</div>
        <div className="experience-input-container">
          <div className="history-info-select">
            <PrimaryDropdown label="Role" overlay={menu} dropdownLabel="Role" />
            <PrimaryDatePicker label="Start Date:" />
          </div>
          <div className="history-info-select">
            <PrimaryDropdown
              label="Company"
              overlay={menu}
              dropdownLabel="Company"
            />
            <PrimaryDatePicker label="End Date:" />
          </div>
        </div>
        <PrimaryTextarea
          label="Overview of responsibilities"
          placeholder="I worked at GIG at a Waiter for many shifts"
        />
      </div>
    );
  }

  function renderExperience() {
    return (
      <div className="experience-container">
        <p>Experience</p>
        <div className="experience-detail">
          <p className="employment-history-title">Employment History</p>
          {renderExperienceHistory()}
          <Divider />
          {renderExperienceHistory()}
        </div>
      </div>
    );
  }

  function renderCvHeader() {
    return (
      <div className="cv-upload-header">
        <p>CV Upload</p>
        <div className="view">
          <Link to={to}>
            <PrimaryButton className={className}>
              <span className="plus-icon">+</span> {buttonContent}
            </PrimaryButton>
          </Link>
        </div>
      </div>
    );
  }

  function renderPdfUpload() {
    return (
      <div className="pdf-file-container">
        <p>
          THIS SECTION WILL SHOW A PREVIEW OF THE PDF OR WORD DOCUMENT THAT WAS
          UPLOADED AT APPLICATION
        </p>
      </div>
    );
  }

  function renderCvUpload() {
    return (
      <div className="experience-container">
        {renderCvHeader()}
        {renderPdfUpload()}
      </div>
    );
  }

  return (
    <div className="modal-info-container">
      {renderExperience()}
      {renderCvUpload()}
    </div>
  );
}

Experience.propTypes = {
  buttonContent: PropTypes.string,
  className: PropTypes.string,
  to: PropTypes.string,
};

export default Experience;
