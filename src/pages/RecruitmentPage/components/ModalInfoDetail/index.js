import React from 'react';

import { Menu, Checkbox, Col, Row, Form } from 'antd';
import PropTypes from 'prop-types';

import PrimaryDropdown from '@/components/PrimaryDropdown';
import PrimaryButton from '@/components/PrimaryButton';
import RadioCheckbox from '@/components/RadioCheckbox';
import PrimaryInput from '@/components/PrimaryInput';
import PrimaryTextarea from '@/components/PrimaryTextarea';

import Experience from '../Experience';

import './styles.scss';

function ModalInfoDetail({ setVisibleModal }) {
  const roles = [
    { label: 'Waiter', value: 'Waiter' },
    { label: 'Bartender', value: 'Bartender' },
    { label: 'Kitchen Porter', value: 'Kitchen Porter' },
    { label: 'Chef', value: 'Chef' },
    { label: 'Room Attendant', value: 'Room Attendant' },
    { label: 'Driver', value: 'Driver' },
    { label: 'Event Staff', value: 'Event Staff' },
    { label: 'Customer Care Steward', value: 'Customer Care Steward' },
    { label: 'Warehouse Operative', value: 'Warehouse Operative' },
    { label: 'Security', value: 'Security' },
    { label: 'Other', value: 'Other' },
  ];

  const menuItems = [
    { item: 'Not enough information' },
    { item: 'Contacted' },
    { item: 'Successful - Auto email' },
    { item: 'Successful - Personal' },
    { item: 'Reject - Permenant' },
    { item: 'Reject - No roles' },
  ];

  const menu = (
    <Menu>
      {menuItems.map((item) => (
        <Menu.Item key={item.item}>{item.item}</Menu.Item>
      ))}
    </Menu>
  );

  const values = [{ value: 'Yes' }, { value: 'No' }];

  const infoInput = [
    {
      label: 'First Name',
      value: 'Isabelle',
    },
    {
      label: 'Last Name',
      value: 'Smith',
    },
    {
      label: 'Email',
      value: 'Isabelle@gmail.com',
    },
    {
      label: 'Phone number',
      value: '077322322',
    },
  ];

  function renderFooterModal() {
    return (
      <div className="modal-footer">
        <div className="footer-item">
          <PrimaryTextarea
            label="Tell us why you want to apply for GIG and what makes you 
            a great candidate?"
            value="abc"
          />
        </div>
        <div className="footer-item">
          <p>
            Some of our roles require working away for 3, 4 or 7 days per week
            with travel and accommodation included. Would you like to be
            considered for these roles?
          </p>
          <RadioCheckbox values={values} />
          <div className="button-container">
            <PrimaryButton
              onClick={() => setVisibleModal(false)}
              className="black-button"
            >
              Close
            </PrimaryButton>
          </div>
        </div>
      </div>
    );
  }

  function renderRolesCheckbox() {
    return (
      <div className="roles-container">
        <p>Roles</p>
        <Checkbox.Group>
          <Row>
            {roles.map((item) => (
              <Col key={item.label} span={4}>
                <Checkbox value={item.value}>{item.label}</Checkbox>
              </Col>
            ))}
          </Row>
        </Checkbox.Group>
      </div>
    );
  }

  return (
    <div className="modal-info-detail-container">
      <p>View - Isabelle Smith</p>
      <Form className="personal-info-container">
        {infoInput.map((info) => (
          <PrimaryInput
            key={info.label}
            label={info.label}
            value={info.value}
          />
        ))}
        <PrimaryDropdown dropdownLabel="Status" overlay={menu} />
      </Form>
      {renderRolesCheckbox()}
      <Experience
        buttonContent="View Full Application"
        className="yellow-button"
        to="/create-new-applicant"
      />
      {renderFooterModal()}
    </div>
  );
}

ModalInfoDetail.propTypes = {
  setVisibleModal: PropTypes.func,
};
export default ModalInfoDetail;
