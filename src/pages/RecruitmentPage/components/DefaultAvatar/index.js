import React from 'react';

import PropTypes from 'prop-types';

import './styles.scss';

function DefaultAvatar({ src }) {
  return (
    <div className="default-ava-container">
      <img src={src} alt="default ava" />
    </div>
  );
}

DefaultAvatar.propTypes = {
  src: PropTypes.string,
};

export default DefaultAvatar;
