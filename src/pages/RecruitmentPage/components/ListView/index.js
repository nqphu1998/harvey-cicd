import React from 'react';

import PropTypes from 'prop-types';
import { Pagination, Table } from 'antd';

import './styles.scss';

function ListView({
  columns,
  infoDetail,
  visibleModal,
  metaData,
  currentPage,
  fetchPage,
  loading,
  setSortRegistrationDate,
}) {
  const onChange = (page) => {
    fetchPage(page);
  };
  return (
    <div className="list-view-container">
      <Table
        columns={columns}
        dataSource={infoDetail}
        pagination={false}
        onRow={(r) => ({
          onClick: () => visibleModal(),
        })}
        loading={loading}
        scroll={{ y: 350 }}
        onChange={(pagination, filters, sorter, extra) =>
          setSortRegistrationDate(sorter.order)
        }
      />
      <Pagination
        current={currentPage}
        total={metaData?.count}
        pageSize={25}
        onChange={onChange}
        hideOnSinglePage
        responsive
      />
    </div>
  );
}

ListView.propTypes = {
  columns: PropTypes.array,
  infoDetail: PropTypes.array,
  visibleModal: PropTypes.func,
  metaData: PropTypes.object,
  currentPage: PropTypes.number,
  fetchPage: PropTypes.func,
  loading: PropTypes.bool,
  setSortRegistrationDate: PropTypes.func,
};
export default ListView;
