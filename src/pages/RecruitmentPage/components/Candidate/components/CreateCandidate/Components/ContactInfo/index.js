import React from 'react';

import { Divider, Col, Form, Row, Tag, Menu } from 'antd';

import defaultAvatar from '@/assets/images/defaultAvatar.png';

import PrimaryButton from '@/components/PrimaryButton';
import PrimaryInput from '@/components/PrimaryInput';
import PrimaryDropdown from '@/components/PrimaryDropdown';
import PrimaryDatePicker from '@/components/PrimaryDatePicker';
import RadioCheckbox from '@/components/RadioCheckbox';

import DefaultAvatar from '@/pages/RecruitmentPage/components/DefaultAvatar';

import './styles.scss';

function ContactInfo() {
  const values = [{ value: 'Yes' }, { value: 'No' }];

  const menuItems = [
    { item: 'Not enough information' },
    { item: 'Contacted' },
    { item: 'Successful - Auto email' },
    { item: 'Successful - Personal' },
    { item: 'Reject - Permenant' },
    { item: 'Reject - No roles' },
  ];

  const menu = (
    <Menu>
      {menuItems.map((item) => (
        <Menu.Item key={item.item}>{item.item}</Menu.Item>
      ))}
    </Menu>
  );

  const leftInfoInput = [
    {
      required: true,
      label: 'First Name',
    },
    {
      required: true,
      label: 'Email',
    },
    {
      required: false,
      label: 'Password',
    },
    {
      required: true,
      label: 'Address - Building',
    },
    {
      required: true,
      label: 'City',
    },
    {
      required: true,
      label: 'Next of Kin - Name and relationship',
    },
  ];

  const rightInfoInput = [
    {
      required: true,
      label: 'Last Name',
    },
    {
      required: true,
      label: 'Phone number',
    },
    {
      required: false,
      label: 'Repeat Password',
    },
    {
      required: true,
      label: 'Street',
    },
    {
      required: true,
      label: 'Postcode',
    },
    {
      required: true,
      label: 'Kext of Kin - Phone and Email',
    },
  ];

  function renderInfoInput() {
    return (
      <Row className="sub-body-row">
        <Col className="sub-body-item">
          {leftInfoInput.map((info) => (
            <PrimaryInput
              key={info.label}
              label={info.label}
              required={info.required}
            />
          ))}
        </Col>
        <Col className="sub-body-item">
          {rightInfoInput.map((info) => (
            <PrimaryInput
              key={info.label}
              label={info.label}
              required={info.required}
            />
          ))}
        </Col>
      </Row>
    );
  }

  function renderInfoSelectLeft() {
    return (
      <Col className="sub-body-item">
        <PrimaryDropdown
          overlay={menu}
          label="Title"
          dropdownLabel="Miss"
          required
        />
        <PrimaryInput label="Nationality" required />
        <PrimaryDatePicker label="Registration" required />
        <Col className="profile-picture-container">
          <p>Profile Picture</p>
          <DefaultAvatar src={defaultAvatar} />
          <PrimaryButton className="white-button">Upload Image</PrimaryButton>
        </Col>
      </Col>
    );
  }

  function renderLanguages() {
    return (
      <Row className="languages-container">
        <Col>
          <h5>Languages</h5>
          <Tag>English - Fluent</Tag>
        </Col>
        <Col>
          <PrimaryButton className="yellow-button">
            <div className="plus-icon">+</div>
            <span>Add Language</span>
          </PrimaryButton>
        </Col>
      </Row>
    );
  }

  function renderInfoSelectRight() {
    return (
      <Col className="sub-body-item">
        <PrimaryDropdown
          overlay={menu}
          label="Marital Status"
          dropdownLabel="Single"
          required
        />
        <PrimaryDatePicker label="Date of Birth" required />
        {renderLanguages()}
        <Divider />
        <Col>
          <p className="criminal-conviction">Criminal Convictions</p>
          <Row className="checkbox-conviction">
            <p>Do you have any convictions</p>
            <RadioCheckbox values={values} />
          </Row>
        </Col>
      </Col>
    );
  }

  function renderInfoSelect() {
    return (
      <Row className="sub-body-row">
        {renderInfoSelectLeft()}
        {renderInfoSelectRight()}
      </Row>
    );
  }

  return (
    <Form className="panel-body">
      <Col className="sub-body">{renderInfoInput()}</Col>
      <Col className="sub-body">{renderInfoSelect()}</Col>
    </Form>
  );
}
export default ContactInfo;
