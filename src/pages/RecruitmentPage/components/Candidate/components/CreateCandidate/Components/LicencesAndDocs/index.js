import React from 'react';

import { Row, Col, Divider } from 'antd';

import defaultAvatar from '@/assets/images/defaultAvatar.png';
import DefaultAvatar from '@/pages/RecruitmentPage/components/DefaultAvatar';
import PrimaryButton from '@/components/PrimaryButton';
import PrimaryDatePicker from '@/components/PrimaryDatePicker';
import PrimaryCheckbox from '@/components/PrimaryCheckbox';

import './styles.scss';

function LicencesAndDocs() {
  const documentType = [
    { label: 'Work permit', value: 'Work permit' },
    {
      label: 'VISA',
      value: 'VISA',
    },
    {
      label: 'Student Letter',
      value: 'Student Letter',
    },
    {
      label: 'IND Application Registration Certificate',
      value: 'IND Application Registration Certificate',
    },
  ];

  const passportInfo = [
    {
      id: 1,
      passportImg: defaultAvatar,
    },
    {
      id: 2,
      passportImg: defaultAvatar,
    },
    {
      id: 3,
      passportImg: defaultAvatar,
    },
  ];

  const workerDocumentation = [
    {
      id: 1,
      avatar: defaultAvatar,
    },
    {
      id: 2,
      avatar: defaultAvatar,
    },
    {
      id: 3,
      avatar: defaultAvatar,
    },
    {
      id: 4,
      avatar: defaultAvatar,
    },
  ];

  const licencesAndTraining = [
    {
      id: 1,
      avatar: defaultAvatar,
    },
    {
      id: 2,
      avatar: defaultAvatar,
    },
    {
      id: 3,
      avatar: defaultAvatar,
    },
    {
      id: 4,
      avatar: defaultAvatar,
    },
    {
      id: 5,
      avatar: defaultAvatar,
    },
    {
      id: 6,
      avatar: defaultAvatar,
    },
    {
      id: 7,
      avatar: defaultAvatar,
    },
    {
      id: 8,
      avatar: defaultAvatar,
    },
    {
      id: 9,
      avatar: defaultAvatar,
    },
    {
      id: 10,
      avatar: defaultAvatar,
    },
    {
      id: 11,
      avatar: defaultAvatar,
    },
    {
      id: 12,
      avatar: defaultAvatar,
    },
  ];

  return (
    <div className="licences-and-docs-container">
      <div className="main-section-container">
        <p>Documents</p>
        <p>ID and Right to Work</p>
        <Row className="images-container">
          {passportInfo.map((passport) => (
            <Col key={passport.id} className="img-item">
              <p>Passport / ID 1</p>
              <DefaultAvatar src={passport.passportImg} />
              <PrimaryButton className="white-button">
                Choose File
              </PrimaryButton>
              <PrimaryDatePicker />
            </Col>
          ))}
          <Col className="img-item">
            <PrimaryCheckbox label="Document Type" options={documentType} />
          </Col>
        </Row>
      </div>
      <Divider />
      <div className="main-section-container">
        <p>Worker Documentation</p>
        <Row className="images-container">
          {workerDocumentation.map((avatar) => (
            <Col key={avatar.id} className="img-item">
              <p>P45</p>
              <Row className="img-button-row">
                <Col>
                  <DefaultAvatar src={avatar.avatar} />
                </Col>
                <Col>
                  <PrimaryButton className="delete-button">
                    Delete
                  </PrimaryButton>
                </Col>
              </Row>
              <PrimaryButton className="white-button">
                Choose File
              </PrimaryButton>
            </Col>
          ))}
        </Row>
      </div>
      <Divider />
      <div className="main-section-container">
        <p>Licences and Training</p>
        <Row className="images-container">
          {licencesAndTraining.map((avatar) => (
            <Col key={avatar.id} className="img-item">
              <p>P45</p>
              <Row className="img-button-row">
                <Col>
                  <DefaultAvatar src={avatar.avatar} />
                </Col>
                <Col>
                  <PrimaryButton className="delete-button">
                    Delete
                  </PrimaryButton>
                </Col>
              </Row>
              <PrimaryButton className="white-button">
                Choose File
              </PrimaryButton>
            </Col>
          ))}
        </Row>
      </div>
    </div>
  );
}
export default LicencesAndDocs;
