import React from 'react';

import { Form, Col, Row, Menu, Divider } from 'antd';

import defaultAvatar from '@/assets/images/defaultAvatar.png';

import PrimaryDropdown from '@/components/PrimaryDropdown';
import PrimaryTextarea from '@/components/PrimaryTextarea';
import PrimaryDatePicker from '@/components/PrimaryDatePicker';
import PrimaryButton from '@/components/PrimaryButton';
import PrimaryCheckbox from '@/components/PrimaryCheckbox';

import DefaultAvatar from '@/pages/RecruitmentPage/components/DefaultAvatar';

import './styles.scss';

function PastWorkDetails() {
  const menuItems = [
    { item: 'Not enough information' },
    { item: 'Contacted' },
    { item: 'Successful - Auto email' },
    { item: 'Successful - Personal' },
    { item: 'Reject - Permenant' },
    { item: 'Reject - No roles' },
  ];

  const dataProtection = [
    { label: 'Verified Store & Use', value: 'Verified Store & Use' },
    {
      label: 'Verified Read Terms & Conditions',
      value: 'Verified Read Terms & Conditions',
    },
    {
      label: 'Accepted Receiving Updates',
      value: 'Accepted Receiving Updates',
    },
  ];

  const roles = [
    { label: 'Waiter', value: 'Waiter' },
    { label: 'Bartender', value: 'Bartender' },
    { label: 'Kitchen Porter', value: 'Kitchen Porter' },
    { label: 'Chef', value: 'Chef' },
    { label: 'Room Attendant', value: 'Room Attendant' },
    { label: 'Warehouse Operative', value: 'Warehouse Operative' },
    { label: 'Event Staff', value: 'Event Staff' },
    { label: 'Customer Care Steward', value: 'Customer Care Steward' },
    { label: 'Driver', value: 'Driver' },
    { label: 'Security', value: 'Security' },
    { label: 'Other', value: 'Other' },
  ];

  const menu = (
    <Menu>
      {menuItems.map((item) => (
        <Menu.Item key={item.item}>{item.item}</Menu.Item>
      ))}
    </Menu>
  );

  function renderExperienceHistory() {
    return (
      <div className="experience-history-container">
        <div className="close-icon">&#215;</div>
        <Col className="experience-input-container">
          <Row className="history-info-select">
            <PrimaryDropdown label="Role" overlay={menu} dropdownLabel="Role" />
            <PrimaryDatePicker label="Start Date:" />
          </Row>
          <Row className="history-info-select">
            <PrimaryDropdown
              label="Company"
              overlay={menu}
              dropdownLabel="Company"
            />
            <PrimaryDatePicker label="End Date:" />
          </Row>
          <PrimaryTextarea label="Overview of responsibilities" value="test" />
        </Col>
      </div>
    );
  }

  function renderAboveSubBodyItem() {
    return (
      <Row className="sub-body-item above">
        <Col className="col-item">
          <p>CV</p>
          <DefaultAvatar src={defaultAvatar} />
          <PrimaryButton className="black-button">CV Upload</PrimaryButton>
          <PrimaryButton className="white-button">Choose File</PrimaryButton>
        </Col>
        <Col className="col-item">
          <PrimaryCheckbox label="Data Protection" options={dataProtection} />
        </Col>
      </Row>
    );
  }

  function renderBelowSubBodyItem() {
    return (
      <Row className="sub-body-item below">
        <Col className="col-item">
          <PrimaryCheckbox label="Roles" options={roles} className="required" />
        </Col>
        <Col className="col-item">
          <p>Specific Role Applied for:</p>
          <PrimaryTextarea
            label="Warehouse GIG Hub"
            placeholder="Other Roles Suitable for"
          />
        </Col>
      </Row>
    );
  }

  return (
    <Form className="panel-body">
      <Col className="sub-body past-work-body">
        {renderExperienceHistory()}
        <Divider />
        {renderExperienceHistory()}
        <Divider />
        <div className="button-container">
          <PrimaryButton className="yellow-button">
            Add New Experience
          </PrimaryButton>
        </div>
      </Col>
      <Col className="sub-body past-work-body white-background">
        {renderAboveSubBodyItem()}
        {renderBelowSubBodyItem()}
      </Col>
    </Form>
  );
}
export default PastWorkDetails;
