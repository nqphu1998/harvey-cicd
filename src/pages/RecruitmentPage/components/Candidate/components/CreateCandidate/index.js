import React from 'react';

import { Divider, Collapse } from 'antd';

import BreadcrumbCustom from '@/components/Breadcrumb';
import PrimaryButton from '@/components/PrimaryButton';

import { RECRUITMENT_VIEW } from '@/constants';

import ContactInfo from './Components/ContactInfo';
import PastWorkDetails from './Components/PastWorkDetails';
import LicencesAndDocs from './Components/LicencesAndDocs';

import './styles.scss';

function CreateCandidate() {
  const { Panel } = Collapse;

  const data = [
    { item: 'Candidate', to: `/recruitment/${RECRUITMENT_VIEW.CANDIDATE}` },
    { item: 'Create New Candidate', to: '/create-new-candidate' },
  ];

  return (
    <div className="create-new-candidate-container">
      <BreadcrumbCustom data={data} mainItem="Recruitment" />
      <div className="create-new-candidate-header">
        <p>Candidate - Isabelle Smith</p>
        <PrimaryButton className="yellow-button">
          <span className="plus-icon">+</span>
          <span>Create Candidate</span>
        </PrimaryButton>
      </div>
      <Divider />
      <div className="create-new-candidate-body">
        <Collapse defaultActiveKey={['Contact Information']}>
          <Panel header="Contact Information" key="Contact Information">
            <ContactInfo />
          </Panel>
          <Panel header="Past Work Details" key="Past Work Details">
            <PastWorkDetails />
          </Panel>
          <Panel header="Licences and Docs" key="Licences and Docs">
            <LicencesAndDocs />
          </Panel>
        </Collapse>
      </div>
    </div>
  );
}
export default CreateCandidate;
