import React, { useState, useCallback } from 'react';

import { Menu, Modal, Form } from 'antd';
import { Link } from 'react-router-dom';

import PrimaryDropdown from '@/components/PrimaryDropdown';
import PrimaryDatePicker from '@/components/PrimaryDatePicker';
import PrimaryButton from '@/components/PrimaryButton';
import PrimaryInput from '@/components/PrimaryInput';
import StatusModal from '@/components/StatusModal';

import { useMergeState } from '@/hooks';

import { ReactComponent as ListIconActive } from '@/assets/icons/listIconActive.svg';
import { ReactComponent as ListIconDeactive } from '@/assets/icons/listIconDeactive.svg';
import { ReactComponent as GridIconActive } from '@/assets/icons/gridIconActive.svg';
import { ReactComponent as GridIconDeactive } from '@/assets/icons/gridIconDeactive.svg';

import ListView from '../../../ListView';
import GridView from '../../../GridView';
import ModalInfoDetail from '../../../ModalInfoDetail';

import './styles.scss';

function CandidateView() {
  const [visibleView, setVisibleView] = useState(false);
  const [visibleModal, setVisibleModal] = useState(false);

  const [visibleStatusModal, setVisibleStatusModal] = useMergeState({
    visible: false,
    type: {
      title: '',
      children: '',
    },
  });

  const { visible, type } = visibleStatusModal;

  const stopPropagation = (e, key) => {
    e.stopPropagation();

    switch (key) {
      case '1':
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Not enough information?',
            children: (
              <>
                <p>Are you sure you do not have enough information?</p>
                <p>An email will be sent to the Applicant to</p>
                <p>request further work and personal information.</p>
              </>
            ),
          },
        });

      case '2':
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Contacted?',
            children: (
              <>
                <p>Have you contacted the applicant?</p>
                <p>No action will be taken on this profile.</p>
              </>
            ),
          },
        });

      case '3':
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Successful - Auto?',
            children: (
              <>
                <p>Are you sure you want to move this applicant forward?</p>
                <p>They will receive an email to book an interview</p>
                <p>and complete full registration.</p>
              </>
            ),
          },
        });

      case '4':
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Successful - Manual?',
            children: (
              <>
                <p>Are you sure you want to mark as successful - Manual?</p>
                <p>No automated email will be sent to the Applicant.</p>
                <p>Only confirm if you have invited the applicant manually.</p>
              </>
            ),
          },
        });

      case '5':
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Rejected - Applicant?',
            children: (
              <>
                <p>Are you sure you want to Reject this applicant?</p>
                <p>They will receive an email their application will</p>
                <p>no longer be available to view.</p>
              </>
            ),
          },
        });

      case '6':
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Rejected - Applicant?',
            children: (
              <>
                <p>Are you sure you want to Reject this applicant?</p>
                <p>They will receive an email their application will</p>
                <p>no longer be available to view.</p>
              </>
            ),
          },
        });

      default:
        return setVisibleStatusModal({
          visible: false,
          type: {
            title: '',
            children: '',
          },
        });
    }
  };

  const menuItems = [
    { item: 'Not enough information' },
    { item: 'Contacted' },
    { item: 'Successful - Auto email' },
    { item: 'Successful - Personal' },
    { item: 'Reject - Permenant' },
    { item: 'Reject - No roles' },
  ];

  const menu = (
    <Menu>
      {menuItems.map((item) => (
        <Menu.Item key={item.item}>{item.item}</Menu.Item>
      ))}
    </Menu>
  );

  const infoDetail = [
    {
      key: 1,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 2,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 3,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 4,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 5,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 6,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 7,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 8,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 9,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 10,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 11,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
    {
      key: 12,
      number: '1',
      name: 'Ella',
      email: 'ella@gmail.com',
      phone: '0123456',
      postcode: '123',
      originalRoles: 'Waiter',
      roles: 'Waiter',
      registration: '1/5/2022',
      location: 'London',
      status: 'test',
    },
  ];

  const handleModal = () => {
    setVisibleModal(!visibleModal);
  };

  const handleVisibleView = useCallback(() => {
    setVisibleView((prevState) => !prevState);
  }, []);

  const columns = [
    {
      title: 'Number',
      dataIndex: 'number',
      key: 'number',
      render: (item) => <div className="ant-table-row">{item}</div>,
    },
    {
      title: 'Full Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Email Address',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'Postcode',
      dataIndex: 'postcode',
      key: 'postcode',
    },
    {
      title: 'Original Roles',
      dataIndex: 'originalRoles',
      key: 'originalRoles',
    },
    {
      title: 'Roles',
      dataIndex: 'roles',
      key: 'roles',
    },
    {
      title: 'Registration Date',
      dataIndex: 'registration',
      key: 'registration',
      defaultSortOrder: 'descend',
      sorter: (a, b) => new Date(a.date) - new Date(b.date),
    },
    {
      title: 'Location',
      dataIndex: 'location',
      key: 'location',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: () => (
        <PrimaryDropdown
          overlay={menu}
          dropdownLabel="Status"
          onClick={stopPropagation}
        />
      ),
    },
  ];

  function renderInfoDetailModal() {
    return (
      <Modal
        centered
        visible={visibleModal}
        footer={['']}
        width={1000}
        onOk={() => setVisibleModal(false)}
        onCancel={() => setVisibleModal(false)}
      >
        <ModalInfoDetail setVisibleModal={setVisibleModal} />
      </Modal>
    );
  }

  function renderApplicationHeader() {
    return (
      <div className="application-header">
        <span>Application</span>
        <div className="create-button">
          <Link to="/create-new-candidate">
            <PrimaryButton className="yellow-button">
              <span className="plus-icon">+</span>
              <span>Create Candidate</span>
            </PrimaryButton>
          </Link>
        </div>
      </div>
    );
  }

  function renderSearchAndDatePicker() {
    return (
      <Form className="info-input-container">
        <PrimaryInput label="Search" placeholder="Name or email address" />
        <div className="date-picker-container">
          <div className="date-picker-item">
            <PrimaryDatePicker label="Registration-date" />
          </div>
          <div className="date-picker-item">
            <PrimaryDatePicker />
          </div>
        </div>
      </Form>
    );
  }

  function renderLocation() {
    return (
      <Form className="info-input-container">
        <div className="select-info-container">
          <div className="select-info">
            <PrimaryDropdown
              label="Other Interested Roles"
              overlay={menu}
              dropdownLabel="Roles"
            />
          </div>
          <div className="select-info">
            <PrimaryDropdown
              label="Location"
              overlay={menu}
              dropdownLabel="Select Location"
            />
          </div>
        </div>
        <div className="select-info-container">
          <div className="select-info">
            <PrimaryDropdown
              label="City Town"
              overlay={menu}
              dropdownLabel="Select Location"
            />
          </div>
          <div className="select-info">
            <PrimaryDropdown
              label="Radius"
              overlay={menu}
              dropdownLabel="Select Radius"
            />
          </div>
        </div>
      </Form>
    );
  }

  function renderRolesAndStatus() {
    return (
      <Form className="info-input-container">
        <PrimaryDropdown
          label="Job Roles Applied"
          overlay={menu}
          dropdownLabel="Name or email address"
        />
        <PrimaryDropdown label="Status" overlay={menu} dropdownLabel="Status" />
      </Form>
    );
  }

  function renderButtonAndIcon() {
    return (
      <div className="info-input-container visible-type">
        <div className="visible-type-item button-container">
          <div className="search-button">
            <PrimaryButton className="yellow-button">Search</PrimaryButton>
          </div>
          <PrimaryButton className="filter-button">Clear Filter</PrimaryButton>
        </div>
        <div className="visible-type-item">
          {visibleView ? (
            <ListIconDeactive onClick={handleVisibleView} />
          ) : (
            <ListIconActive onClick={handleVisibleView} />
          )}
          {visibleView ? (
            <GridIconActive onClick={handleVisibleView} />
          ) : (
            <GridIconDeactive onClick={handleVisibleView} />
          )}
        </div>
      </div>
    );
  }

  return (
    <div className="recruitment-container">
      <StatusModal
        visible={visible}
        title={type.title}
        onClickNo={() => setVisibleStatusModal({ visible: false })}
      >
        {type.children}
      </StatusModal>
      {visibleModal && renderInfoDetailModal()}
      <div className="application-detail-container">
        {renderApplicationHeader()}
        <div className="application-info-input-container">
          {renderSearchAndDatePicker()}
          {renderRolesAndStatus()}
          {renderLocation()}
          {renderButtonAndIcon()}
        </div>
        {visibleView ? (
          <GridView
            infoDetail={infoDetail}
            visibleModal={handleModal}
            stopPropagation={stopPropagation}
          />
        ) : (
          <ListView
            columns={columns}
            infoDetail={infoDetail}
            visibleModal={handleModal}
          />
        )}
        <div className="download-button-container">
          <div className="download-button">
            <PrimaryButton className="yellow-button">
              Download CSV
            </PrimaryButton>
          </div>
        </div>
      </div>
    </div>
  );
}
export default CandidateView;
