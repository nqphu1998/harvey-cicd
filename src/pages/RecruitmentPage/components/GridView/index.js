import React from 'react';

import { Card } from 'antd';
import PropTypes from 'prop-types';

import './styles.scss';

function GridView({ visibleModal, infoDetail }) {
  function renderGridColumn({ title, data }) {
    return (
      <div className="grid-column-container">
        <div className="column-header">{title}</div>
        <div className="column-body">
          {data.map((dt) => (
            <Card
              onClick={visibleModal}
              className="info-detail-container"
              key={dt.key}
            >
              <p>{dt.number}</p>
              <p>{dt.name}</p>
              <p>{dt.email}</p>
              <p>{dt.phone}</p>
              <p>{dt.postcode}</p>
              <p>{dt.roles}</p>
              <p>{dt.location}</p>
            </Card>
          ))}
        </div>
      </div>
    );
  }

  return (
    <div className="grid-view-container">
      {renderGridColumn({ title: 'Applied', data: infoDetail })}
      {renderGridColumn({ title: 'Candidate', data: infoDetail })}
      {renderGridColumn({ title: 'Done', data: infoDetail })}
    </div>
  );
}

GridView.propTypes = {
  infoDetail: PropTypes.array,

  visibleModal: PropTypes.func,
};
export default GridView;
