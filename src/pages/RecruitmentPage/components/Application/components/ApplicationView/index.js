import React, { useState, useCallback, useEffect } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';

import { Menu, Modal, Form } from 'antd';
import { Link } from 'react-router-dom';

import PrimaryDropdown from '@/components/PrimaryDropdown';
import PrimaryDatePicker from '@/components/PrimaryDatePicker';
import PrimaryButton from '@/components/PrimaryButton';
import PrimaryInput from '@/components/PrimaryInput';
import StatusModal from '@/components/StatusModal';

import { useMergeState } from '@/hooks';

import { ReactComponent as ListIconActive } from '@/assets/icons/listIconActive.svg';
import { ReactComponent as ListIconDeactive } from '@/assets/icons/listIconDeactive.svg';
import { ReactComponent as GridIconActive } from '@/assets/icons/gridIconActive.svg';
import { ReactComponent as GridIconDeactive } from '@/assets/icons/gridIconDeactive.svg';

import { selectApplications, selectApplicationsStatus, selectMetaDataApplications } from '@/store/application/applicationSelector';
import { ApplicationCandidateStatus } from '@/constants';
import { isPending } from '@/utils/loadingUtil';
import { getApplications } from '@/store/application/applicationThunk';

import ListView from '../../../ListView';
import GridView from '../../../GridView';
import ModalInfoDetail from '../../../ModalInfoDetail';

import './styles.scss';

const convertSortRegistrationsDate = (sort) => {
  if (sort === 'descend') return 'desc';
  if (sort === 'ascend') return 'asc';
  return null;
};

function ApplicationView() {
  const [visibleView, setVisibleView] = useState(false);
  const [visibleModal, setVisibleModal] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const loading = useSelector(selectApplicationsStatus);
  const dispatch = useDispatch();
  const [sortRegistrationDate, setSortRegistrationDate] = useState('descend');
  const applications = useSelector(selectApplications, shallowEqual) || [];
  const metaData = useSelector(selectMetaDataApplications, shallowEqual);

  const fetchPage = useCallback(
    (page) => {
      setCurrentPage(page);
      dispatch(
        getApplications({
          page,
          registration_date: convertSortRegistrationsDate(sortRegistrationDate),
        }),
      );
    },
    [dispatch, sortRegistrationDate],
  );

  useEffect(() => {
    dispatch(
      getApplications({
        page: 1,
        registration_date: convertSortRegistrationsDate(sortRegistrationDate),
      }),
    );
  }, [dispatch, sortRegistrationDate]);

  const [visibleStatusModal, setVisibleStatusModal] = useMergeState({
    visible: false,
    type: {
      title: '',
      children: '',
    },
  });

  const { visible, type } = visibleStatusModal;

  const handleStatus = (e, key) => {
    e.stopPropagation();

    switch (key) {
      case ApplicationCandidateStatus.NOT_ENOUGH_INFORMATION:
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Not enough information?',
            children:
              'Are you sure you do not have enough information? An email will be sent to the Applicant to request further work and personal information.',
          },
        });

      case ApplicationCandidateStatus.CONTACTED:
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Contacted?',
            children:
              'Have you contacted the applicant? No action will be taken on this profile.',
          },
        });

      case ApplicationCandidateStatus.SUCCESSFUL_AUTO_EMAIL:
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Successful - Auto?',
            children:
              'Are you sure you want to move this applicant forward? They will receive an email to book an interview and complete full registration.',
          },
        });

      case ApplicationCandidateStatus.SUCCESSFUL_PERSONAL:
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Successful - Manual?',
            children:
              'Are you sure you want to mark as successful - Manual? No automated email will be sent to the Applicant. Only confirm if you have invited the applicant manually.',
          },
        });

      case ApplicationCandidateStatus.REJECT_PERMENANT:
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Rejected - Applicant?',
            children:
              'Are you sure you want to Reject this applicant? They will receive an email their application will no longer be available to view.',
          },
        });

      case ApplicationCandidateStatus.REJECT_NO_ROLES:
        return setVisibleStatusModal({
          visible: true,
          type: {
            title: 'Rejected - Applicant?',
            children:
              'Are you sure you want to Reject this applicant? They will receive an email their application will no longer be available to view.',
          },
        });

      default:
        return setVisibleStatusModal({
          visible: false,
          type: {
            title: '',
            children: '',
          },
        });
    }
  };

  const menuItems = [
    { item: 'Not enough information' },
    { item: 'Contacted' },
    { item: 'Successful - Auto email' },
    { item: 'Successful - Personal' },
    { item: 'Reject - Permenant' },
    { item: 'Reject - No roles' },
  ];

  const menu = (
    <Menu onClick={({ domEvent, key }) => handleStatus(domEvent, key)}>
      {menuItems.map((item) => (
        <Menu.Item key={item.item}>{item.item}</Menu.Item>
      ))}
    </Menu>
  );


  const handleModal = () => {
    setVisibleModal(!visibleModal);
  };

  const handleVisibleView = useCallback(() => {
    setVisibleView((prevState) => !prevState);
  }, []);

  const columns = [
    {
      title: 'Number',
      dataIndex: 'number',
      key: 'number',
      render: (item) => <div className="ant-table-row">{item}</div>,
    },
    {
      title: 'Full Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Email Address',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'Postcode',
      dataIndex: 'postcode',
      key: 'postcode',
    },
    {
      title: 'Original Roles',
      dataIndex: 'originalRoles',
      key: 'originalRoles',
    },
    {
      title: 'Other Roles',
      dataIndex: 'otherRoles',
      key: 'otherRoles',
    },
    {
      title: 'Registration Date',
      dataIndex: 'registration',
      key: 'registration',
      defaultSortOrder: 'descend',
      sorter: (a, b) => new Date(a.date) - new Date(b.date),
    },
    {
      title: 'Location',
      dataIndex: 'location',
      key: 'location',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: () => (
        <PrimaryDropdown
          overlay={menu}
          dropdownLabel="Status"
          onClick={handleStatus}
        />
      ),
    },
  ];

  function renderInfoDetailModal() {
    return (
      <Modal
        centered
        visible={visibleModal}
        footer={['']}
        width={1000}
        onOk={() => setVisibleModal(false)}
        onCancel={() => setVisibleModal(false)}
      >
        <ModalInfoDetail setVisibleModal={setVisibleModal} />
      </Modal>
    );
  }

  function renderApplicationHeader() {
    return (
      <div className="application-header">
        <span>Application</span>
        <div className="create-button">
          <Link to="/create-new-application">
            <PrimaryButton className="yellow-button">
              <span className="plus-icon">+</span>
              <span>Create Application</span>
            </PrimaryButton>
          </Link>
        </div>
      </div>
    );
  }

  function renderSearchAndDatePicker() {
    return (
      <Form className="info-input-container">
        <PrimaryInput label="Search" placeholder="Name or email address" />
        <div className="applicant-date-picker__container">
          <p className="applicant-date-picker__label">Registration-date</p>
          <div className="applicant-date-picker__inner">
            <div className="applicant-date-picker__item">
              <PrimaryDatePicker />
            </div>
            <div className="applicant-date-picker__item">
              <PrimaryDatePicker />
            </div>
          </div>
        </div>
      </Form>
    );
  }

  function renderLocation() {
    return (
      <Form className="info-input-container" style={{ flex: 2 }}>
        <div className="select-info-container">
          <div className="select-info">
            <PrimaryDropdown
              label="Other Interested Roles"
              overlay={menu}
              dropdownLabel="Roles"
            />
          </div>
          <div className="select-info">
            <PrimaryDropdown
              label="Location"
              overlay={menu}
              dropdownLabel="Select Location"
            />
          </div>
        </div>
        <div className="select-info-container">
          <div className="select-info">
            <PrimaryDropdown
              label="City Town"
              overlay={menu}
              dropdownLabel="Select Location"
            />
          </div>
          <div className="select-info">
            <PrimaryDropdown
              label="Radius"
              overlay={menu}
              dropdownLabel="Select Radius"
            />
          </div>
        </div>
      </Form>
    );
  }

  function renderRolesAndStatus() {
    return (
      <Form className="info-input-container">
        <PrimaryDropdown
          label="Job Roles Applied"
          overlay={menu}
          dropdownLabel="Name or email address"
        />
        <PrimaryDropdown label="Status" overlay={menu} dropdownLabel="Status" />
      </Form>
    );
  }

  function renderButtonAndIcon() {
    return (
      <div className="info-input-container visible-type" style={{ flex: 0.8 }}>
        <div className="visible-type-item button-container">
          <div className="search-button">
            <PrimaryButton className="yellow-button">Search</PrimaryButton>
          </div>
          <PrimaryButton className="filter-button">Clear Filter</PrimaryButton>
        </div>
        <div className="visible-type-item">
          {visibleView ? (
            <ListIconDeactive onClick={handleVisibleView} />
          ) : (
            <ListIconActive />
          )}
          {visibleView ? (
            <GridIconActive />
          ) : (
            <GridIconDeactive onClick={handleVisibleView} />
          )}
        </div>
      </div>
    );
  }

  return (
    <div className="recruitment-container">
      <StatusModal
        visible={visible}
        title={type.title}
        onClickNo={() => setVisibleStatusModal({ visible: false })}
      >
        {type.children}
      </StatusModal>
      {visibleModal && renderInfoDetailModal()}
      <div className="application-detail-container">
        {renderApplicationHeader()}
        <div className="application-info-input-container">
          {renderSearchAndDatePicker()}
          {renderRolesAndStatus()}
          {renderLocation()}
          {renderButtonAndIcon()}
        </div>
        {visibleView ? (
          <GridView
            infoDetail={applications}
            visibleModal={handleModal}
            handleStatus={handleStatus}
          />
        ) : (
          <ListView
            columns={columns}
            infoDetail={applications}
            visibleModal={handleModal}
            metaData={metaData}
            currentPage={currentPage}
            fetchPage={fetchPage}
            loading={isPending(loading)}
            setSortRegistrationDate={setSortRegistrationDate}
          />
        )}
        <div className="download-button-container">
          <div className="download-button">
            <PrimaryButton className="yellow-button">
              Download CSV
            </PrimaryButton>
          </div>
        </div>
      </div>
    </div>
  );
}
export default ApplicationView;
