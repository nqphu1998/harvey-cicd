import React from 'react';

import { Menu, Divider, Form } from 'antd';

import BreadcrumbCustom from '@/components/Breadcrumb';
import PrimaryDropdown from '@/components/PrimaryDropdown';
import PrimaryInput from '@/components/PrimaryInput';
import PrimaryTextarea from '@/components/PrimaryTextarea';
import PrimaryButton from '@/components/PrimaryButton';
import RadioCheckbox from '@/components/RadioCheckbox';
import PrimaryCheckbox from '@/components/PrimaryCheckbox';

import { RECRUITMENT_VIEW } from '@/constants';

import Experience from '../../../Experience';

import './styles.scss';

function CreateApplication() {
  const data = [
    { item: 'Application', to: `/recruitment/${RECRUITMENT_VIEW.APPLICATION}` },
    { item: 'Create New Application', to: '/create-new-application' },
  ];

  const roles = [
    { label: 'Waiter', value: 'Waiter' },
    { label: 'Bartender', value: 'Bartender' },
    { label: 'Kitchen Porter', value: 'Kitchen Porter' },
    { label: 'Chef', value: 'Chef' },
    { label: 'Room Attendant', value: 'Room Attendant' },
    { label: 'Warehouse Operative', value: 'Warehouse Operative' },
    { label: 'Event Staff', value: 'Event Staff' },
    { label: 'Customer Care Steward', value: 'Customer Care Steward' },
    { label: 'Driver', value: 'Driver' },
    { label: 'Security', value: 'Security' },
    { label: 'Other', value: 'Other' },
  ];

  const menuItems = [
    { item: 'Not enough information' },
    { item: 'Contacted' },
    { item: 'Successful - Auto email' },
    { item: 'Successful - Personal' },
    { item: 'Reject - Permenant' },
    { item: 'Reject - No roles' },
  ];

  const menu = (
    <Menu>
      {menuItems.map((item) => (
        <Menu.Item key={item.item}>{item.item}</Menu.Item>
      ))}
    </Menu>
  );

  const values = [{ value: 'Yes' }, { value: 'No' }];

  function renderContactDetails() {
    return (
      <Form className="info-input-container fixed-height">
        <div className="info-input">
          <PrimaryInput label="First Name" placeholder="First name" />
          <PrimaryInput label="Email" placeholder="Email" />
          <PrimaryDropdown
            label="Location"
            dropdownLabel="Choose your local area"
            overlay={menu}
          />
        </div>
        <div className="info-input">
          <PrimaryInput label="Last Name" placeholder="Last name" />
          <PrimaryInput label="Phone number" placeholder="Phone number" />
        </div>
      </Form>
    );
  }

  function renderRolesInput() {
    return (
      <div className="info-input-container">
        <PrimaryCheckbox label="Roles" options={roles} />
        <div className="roles-container">
          <p>Specific Role Applied for:</p>
          <PrimaryTextarea
            label="Warehouse GIG Hub"
            placeholder="Other Roles Suitable for"
          />
        </div>
      </div>
    );
  }

  return (
    <div className="create-new-applicant-container">
      <BreadcrumbCustom data={data} mainItem="Recruitment" />
      <div className="create-new-header">
        <span>Create New Application</span>
      </div>
      <div className="create-new-body">
        <div className="body-container">
          <div className="section-container">
            <p>Contact Details</p>
            {renderContactDetails()}
            <Divider />
            {renderRolesInput()}
            <Divider />
          </div>
          <div className="section-container">
            <div className="sub-section-container fixed-height">
              <p>
                Some of our roles require working away for 3, 4 or 7 days per
                week with travel and accommodation included. Would you like to
                be considered for these roles?
              </p>
              <RadioCheckbox values={values} />
              <PrimaryTextarea
                label="Tell us why you want to apply for GIG and what makes you 
                a great candidate?"
                placeholder="I want to work for GIG because I think that I will be 
              a great addition to the team I am a hard worker and have experience 
              in the Warehouse Industry."
              />
            </div>
            <div className="sub-section-container">
              <div className="status-dropdown-container">
                <PrimaryDropdown label="Status" placeholder="Select Status" />
              </div>
              <PrimaryTextarea label="Internal note" placeholder="note" />
            </div>
          </div>
        </div>
        <div className="body-container">
          <Experience buttonContent="CV upload" className="black-button" />
        </div>
        <Divider />
        <div className="button-footer">
          <div className="button-container">
            <PrimaryButton className="black-button">Cancel</PrimaryButton>
          </div>
          <div className="button-container">
            <PrimaryButton className="yellow-button">Save</PrimaryButton>
          </div>
        </div>
      </div>
    </div>
  );
}
export default CreateApplication;
