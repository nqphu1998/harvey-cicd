import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { Form, Checkbox } from 'antd';

import PrimaryButton from '@/components/PrimaryButton';
import PrimaryInput from '@/components/PrimaryInput';

import { login } from '@/store/auth/authThunk';
import { selectAuth } from '@/store/auth/authSelector';

import { isPending } from '@/utils/loadingUtil';

import gigLogo from '@/assets/images/gigLogo.png';
import admin from '@/assets/images/admin.png';

import './index.scss';

function LoginPage() {
  const dispatch = useDispatch();
  const auth = useSelector(selectAuth);

  const handleLogin = (credentials) => {
    dispatch(login(credentials));
  };

  if (auth.token) {
    return <Redirect to="/" />;
  }

  return (
    <div className="login-page">
      <img className="gig-logo" src={gigLogo} alt="gig-logo" />
      <img className="admin-logo" src={admin} alt="admin" />
      <div className="form">
        <Form
          className="input-container"
          name="login"
          layout="vertical"
          onFinish={handleLogin}
          initialValues={{
            email: '',
            password: '',
          }}
        >
          <PrimaryInput
            label="Email"
            placeholder="Input your email"
            name="email"
            required
            rules={[
              { type: 'email', message: 'Email is invalid' },
              { required: true, message: 'Email is required' },
            ]}
          />
          {auth.error && (
            <p className="error-msg">Your email or password is incorrect</p>
          )}
          <PrimaryInput
            label="Password"
            placeholder="Input your password"
            name="password"
            type="password"
            required
            rules={[{ required: true, message: 'Password is required' }]}
          />
          <PrimaryButton
            htmlType="submit"
            loading={isPending(auth.loading)}
            className="black-button"
          >
            Login
          </PrimaryButton>
          <Link className="forgot-password" to="">
            Forgot Password
          </Link>
        </Form>
        <Checkbox className="remember-me">Remember me</Checkbox>
      </div>
      <div className="login-text-container">
        <p>
          GIG the leading Staffing and Account Management Platform and service
          provider,
        </p>
        <p>to help assist with all your staffing needs.</p>
        <p className="text-divider">
          Download the GIG App from Apple or Play Store or visit
          www.gigprovider.co.uk to post jobs today.
        </p>
        <p>
          If you do not have a profile and require Staff Support please email
          Sales@gigtogig.co.uk
        </p>
      </div>
    </div>
  );
}

export default LoginPage;
