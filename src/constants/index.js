const ENV = {
  API_BASE_URL: process.env.REACT_APP_API_BASE_URL,
};

const LoadingStatus = {
  idle: 'idle',
  pending: 'pending',
  fulfilled: 'fulfilled',
  rejected: 'rejected',
};

const StorageKey = {
  authUser: '@auth:user',
  authToken: '@auth:token',
};

const RECRUITMENT_VIEW = {
  APPLICATION: 'application',
  CANDIDATE: 'candidate',
};

const ApplicationCandidateStatus = {
  NOT_ENOUGH_INFORMATION: 'Not enough information',
  CONTACTED: 'Contacted',
  SUCCESSFUL_AUTO_EMAIL: 'Successful - Auto email',
  SUCCESSFUL_PERSONAL: 'Successful - Personal',
  REJECT_PERMENANT: 'Reject - Permenant',
  REJECT_NO_ROLES: 'Reject - No roles',
};

export {
  ENV,
  LoadingStatus,
  StorageKey,
  ApplicationCandidateStatus,
  RECRUITMENT_VIEW,
};
