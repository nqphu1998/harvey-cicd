import http from '@/http';
import { ServiceError } from '@/utils/errorUtil';
import normalize from 'json-api-normalizer';

export async function getApplications(params) {
  try {
    const res = await http.get('api/applications', { params });

    return {
      applications: normalize(res.data),
      meta: res.data.meta.pagy,
    }; 
  } catch (error) {
    console.error(error);
    throw new ServiceError({
      message: 'Get applications fails',
      details: ['Get applications fails'],
    });
  }
}