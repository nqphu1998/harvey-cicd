import http from '@/http';
import { ServiceError } from '@/utils/errorUtil';

export async function login({ email, password }) {
  try {
    const { data } = await http.post('api/users/login', {
      user: {
        email,
        password,
      },
    });
    const { token } = data.links;
    return token;
  } catch (error) {
    console.error(error);
    throw new ServiceError({
      message: 'Unauthorize!',
      details: ['Invalid email or password'],
    });
  }
}

export async function logout() {
  try {
    await http.post('api/users/logout');
  } catch (error) {
    console.error(error);
  }
}
