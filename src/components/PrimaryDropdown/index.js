import React from 'react';

import classNames from 'classnames';
import PropTypes from 'prop-types';
import { DownOutlined } from '@ant-design/icons';
import { Button, Dropdown, Form } from 'antd';

import './styles.scss';

function PrimaryDropdown({ overlay, dropdownLabel, onClick, required, label }) {
  return (
    <Form.Item
      className={classNames('shared-dropdown-container', { required })}
      label={label}
    >
      <Dropdown
        className="dropdown-button"
        onClick={onClick}
        overlay={overlay}
      >
        <Button className="dropdown-button">
          {dropdownLabel} <DownOutlined />
        </Button>
      </Dropdown>
    </Form.Item>
  );
}

PrimaryDropdown.propTypes = {
  overlay: PropTypes.node,
  dropdownLabel: PropTypes.string,
  label: PropTypes.string,
  onClick: PropTypes.func,
  required: PropTypes.bool,
};
export default PrimaryDropdown;
