import React from 'react';

import { Form, Input } from 'antd';
import PropTypes from 'prop-types';

import './styles.scss';

function PrimaryTextarea({ label, placeholder, value }) {
  return (
    <Form.Item label={label} className='textarea-container'>
      <Input.TextArea placeholder={placeholder} value={value} />
    </Form.Item>
  );
}

PrimaryTextarea.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
};

export default PrimaryTextarea;
