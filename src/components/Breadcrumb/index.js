import React from 'react';

import PropTypes from 'prop-types';
import { Breadcrumb } from 'antd';

import { Link } from 'react-router-dom';

import { ReactComponent as HomeIcon } from '@/assets/icons/homeIcon.svg';

import './styles.scss';

function BreadcrumbCustom({ data, mainItem }) {
  // Example: data = [{item: 'Candidate'}]

  return (
    <Breadcrumb separator="" className="breadcrumb-container">
      <Breadcrumb.Item>{mainItem}</Breadcrumb.Item>
      <Breadcrumb.Separator>
        <div className="separator">|</div>
      </Breadcrumb.Separator>
      <Breadcrumb.Item>
        <Link to="/">
          <HomeIcon className="home-icon" />
        </Link>
      </Breadcrumb.Item>
      {data.map((dt) => (
        <Breadcrumb.Item className="breadcrumb-item" key={dt.item}>
          <Link to={dt.to}>
            <span className="dash">&#8212;</span> {dt.item}
          </Link>
        </Breadcrumb.Item>
      ))}
    </Breadcrumb>
  );
}

BreadcrumbCustom.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string,
      item: PropTypes.string,
    }),
  ),
  mainItem: PropTypes.string,
};

export default BreadcrumbCustom;
