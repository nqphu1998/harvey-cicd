import React from 'react';
import { useDispatch } from 'react-redux';
import { Link, useLocation, useParams } from 'react-router-dom';

import { Menu, Collapse } from 'antd';
import classNames from 'classnames';

import { RECRUITMENT_VIEW } from '@/constants';

import { logout } from '@/store/auth/authThunk';

import gigLogo from '@/assets/images/gigLogo.png';
import defaultAvatar from '@/assets/images/defaultAvatar.png';

import './styles.scss';

function Header() {
  const dispatch = useDispatch();

  const { Panel } = Collapse;

  const { pathname } = useLocation();

  const { view } = useParams();

  const [collapseValue, setCollapseValue] = React.useState();

  const menuItems = [
    { item: 'Seeker', to: '/seeker' },
    { item: 'Provider', to: '/provider' },
    { item: 'Jobs', to: '/job' },
    { item: 'Timesheets', to: '/timesheets' },
    { item: 'Payroll', to: '/payroll' },
    { item: 'Reports', to: '/report' },
    { item: 'Users', to: '/users' },
  ];

  const handleCollapseRecruitment = () => {
    if (pathname === `/recruitment/${view}`) {
      return 'recruitment';
    }
    return null;
  };

  const handleLogout = () => {
    dispatch(logout());
  };

  function renderRecruitment() {
    return (
      <Menu.Item
        className={classNames({
          active: pathname === `/recruitment/${view}`,
        })}
        key="recruitment"
        onClick={() => setCollapseValue('recruitment')}
      >
        <Collapse
          accordion
          activeKey={collapseValue}
          defaultActiveKey={handleCollapseRecruitment()}
          ghost
        >
          <Panel
            showArrow={false}
            header="Recruitment"
            key="recruitment"
            forceRender
          >
            <Link
              className={classNames({
                active:
                  pathname === `/recruitment/${RECRUITMENT_VIEW.APPLICATION}`,
              })}
              to={`/recruitment/${RECRUITMENT_VIEW.APPLICATION}`}
            >
              {RECRUITMENT_VIEW.APPLICATION}
            </Link>
            <Link
              className={classNames({
                active:
                  pathname === `/recruitment/${RECRUITMENT_VIEW.CANDIDATE}`,
              })}
              to={`/recruitment/${RECRUITMENT_VIEW.CANDIDATE}`}
            >
              {RECRUITMENT_VIEW.CANDIDATE}
            </Link>
          </Panel>
        </Collapse>
      </Menu.Item>
    );
  }

  function renderMenuItems() {
    return menuItems.map((item) => (
      <Menu.Item onClick={() => setCollapseValue('')} key={item.item}>
        <Link
          className={classNames({
            active: pathname === item.to,
          })}
          to={item.to}
        >
          {item.item}
        </Link>
      </Menu.Item>
    ));
  }

  function renderAvatar() {
    return (
      <Menu.SubMenu
        popupClassName="dropdown-container"
        key="submenu"
        title={
          <div className="avatar-container">
            <img
              className="default-avatar"
              src={defaultAvatar}
              alt="default-avatar"
            />
          </div>
        }
      >
        <div key="avatar" className="submenu-item-container">
          <Menu.Item className="submenu-item">View Profile</Menu.Item>
          <Menu.Divider />
          <Menu.Item onClick={handleLogout} className="submenu-item">
            Log out
          </Menu.Item>
        </div>
      </Menu.SubMenu>
    );
  }

  return (
    <div className="header-container">
      <Link to="/">
        <img className="header-logo" src={gigLogo} alt="gig-logo" />
      </Link>
      <Menu
        className="menu-container"
        mode="horizontal"
        triggerSubMenuAction="click"
      >
        <Menu.Item key="/" onClick={() => setCollapseValue('')}>
          <Link
            className={classNames({
              active: pathname === '/',
            })}
            to="/"
          >
            Dashboard
          </Link>
        </Menu.Item>
        {renderRecruitment()}
        {renderMenuItems()}
        {renderAvatar()}
      </Menu>
    </div>
  );
}

export default Header;
