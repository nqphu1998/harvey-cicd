import React from 'react';

import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Input, Form } from 'antd';

import './styles.scss';

function PrimaryInput({
  label,
  placeholder,
  value,
  required,
  rules,
  name,
  type,
}) {
  return (
    <Form.Item
      name={name}
      label={label}
      className={classNames('primary-input-container', { required })}
      rules={rules}
    >
      <Input type={type} value={value} placeholder={placeholder} />
    </Form.Item>
  );
}

PrimaryInput.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  required: PropTypes.bool,
  rules: PropTypes.array,
};

export default PrimaryInput;
