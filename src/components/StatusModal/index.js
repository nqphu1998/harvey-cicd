import React from 'react';

import { Modal } from 'antd';
import PropTypes from 'prop-types';

import PrimaryButton from '@/components/PrimaryButton';

import { ReactComponent as WarningIcon } from '@/assets/icons/warningIcon.svg';

import './styles.scss';

function StatusModal({ title, children, visible, onClickNo, onClickYes }) {
  return (
    <Modal
      visible={visible}
      centered
      onCancel={onClickNo}
      footer={[
        <PrimaryButton
          onClick={onClickYes}
          type="primary"
          key="yes"
          className="yellow-button"
        >
          Yes
        </PrimaryButton>,
        <PrimaryButton
          onClick={onClickNo}
          type="primary"
          key="no"
          className="black-button"
        >
          No
        </PrimaryButton>,
      ]}
      className="status-modal-container"
    >
      <WarningIcon className="warning-icon" />
      <h1>{title}</h1>
      <p>{children}</p>
    </Modal>
  );
}

StatusModal.propTypes = {
  title: PropTypes.string,
  children: PropTypes.string,
  visible: PropTypes.bool,
  onClickNo: PropTypes.func,
  onClickYes: PropTypes.func,
};

export default StatusModal;
