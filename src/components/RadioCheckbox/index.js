import React from 'react';

import { Radio } from 'antd';
import PropTypes from 'prop-types';

import './styles.scss';

function RadioCheckbox({ values }) {
  // Note: values = [{value: 'Yes}, {value: 'No'}]

  return (
    <Radio.Group className="radio-checkbox-container">
      {values.map((value) => (
        <Radio key={value} value={value.value}>{value.value}</Radio>
      ))}
    </Radio.Group>
  );
}

RadioCheckbox.propTypes = {
  values: PropTypes.array,
};

export default RadioCheckbox;
