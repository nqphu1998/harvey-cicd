import React from 'react';

import PropTypes from 'prop-types';
import { Button } from 'antd';

import './styles.scss';

function PrimaryButton({ children, className, onClick, loading, htmlType }) {
  return (
    <Button onClick={onClick} className={className} htmlType={htmlType} loading={loading}>
      {children}
    </Button>
  );
}

PrimaryButton.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
  loading: PropTypes.bool,
  htmlType: PropTypes.string,
};
export default PrimaryButton;
