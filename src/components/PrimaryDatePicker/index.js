import React from 'react';

import { DatePicker, Form } from 'antd';
import moment from 'moment';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { ReactComponent as Calendar } from '@/assets/icons/calendarIcon.svg';

import './styles.scss';

function PrimaryDatePicker({ label, required }) {
  return (
    <Form.Item
      className={classNames('shared-date-picker-container', { required })}
      label={label}
    >
      <DatePicker
        className="date-picker"
        defaultValue={moment(new Date(), 'DD/MM/YYYY')}
        format="DD/MM/YYYY"
        suffixIcon={<Calendar />}
      />
    </Form.Item>
  );
}

PrimaryDatePicker.propTypes = {
  label: PropTypes.string,
  required: PropTypes.bool,
};

export default PrimaryDatePicker;
