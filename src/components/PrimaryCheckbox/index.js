import React from 'react';

import { Checkbox } from 'antd';
import PropTypes from 'prop-types';

import './styles.scss';

function PrimaryCheckbox({ label, options, className }) {
  return (
    <div className="primary-checkbox-container">
      <p className={className}>{label}</p>
      <Checkbox.Group options={options} />
    </div>
  );
}

PrimaryCheckbox.propTypes = {
  label: PropTypes.node.isRequired,
  options: PropTypes.array,
  className: PropTypes.string,
};

export default PrimaryCheckbox;
