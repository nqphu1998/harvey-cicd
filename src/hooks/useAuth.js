import { useSelector } from 'react-redux';
import { selectAuth } from '@/store/auth/authSelector';

const useAuth = () => {
  const { token } = useSelector(selectAuth);

  return {
    isAuth: !!token,
    token,
  };
};

export default useAuth;

