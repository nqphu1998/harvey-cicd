import { LoadingStatus } from '@/constants';
import { removeAuthUser } from '@/utils/localStorageUtil';
import { createSlice } from '@reduxjs/toolkit';
import { getApplications } from './applicationThunk';

const initialState = {
  loading: LoadingStatus.idle,
  error: null,
  applications: null,
  meta:null
};

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    clear(state) {
      state.loading = LoadingStatus.idle;
      state.applications = null;
      state.meta = null;
      state.error = null;
    },
  },
  extraReducers: {
    [getApplications.pending]: (state) => {
      state.loading = LoadingStatus.pending;
    },
    [getApplications.fulfilled]: (state, { payload}) => {
      state.loading = LoadingStatus.fulfilled;
      state.error = null;
      state.applications = payload.applications;
      state.meta = payload.meta;
    },
    [getApplications.rejected]: (state, { payload: error }) => {
      state.loading = LoadingStatus.rejected;
      state.error = error;
      state.applications = null;
    },
  },
});

export const {  clear } = slice.actions;
export default slice.reducer;

// Extra Actions

export const logout = () => async (dispatch) => {
  removeAuthUser();
  dispatch(clear());
};
