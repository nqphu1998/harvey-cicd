import { ApplicationsService } from '@/services';
import { errorToObject } from '@/utils/errorUtil';
import { createAsyncThunk } from '@reduxjs/toolkit';

export const getApplications = createAsyncThunk(
  'applications/index',
  async (params, thunkAPI) => {
    try {
      const listApplications = await ApplicationsService.getApplications(params);
      return listApplications;
    } catch (error) {
      return thunkAPI.rejectWithValue(errorToObject(error));
    }
  },
);
