import { ApplicationCandidateStatus } from '@/constants';
import { format } from 'date-fns';

export const selectApplications = (state) => {
  const vacancyApplicant = Object.values(
    state?.applications?.applications?.vacancyApplicant || {},
  );

  return vacancyApplicant.map(({ id, attributes }) => {
    const { decodeAttributes, location, createdAt, status } = attributes;
    const { firstname, lastname, email, phone, zip } = decodeAttributes;
    return {
      key: id,
      number: id,
      name: `${firstname}+${lastname}`,
      email,
      phone,
      postcode: zip,
      originalRoles:'',
      otherRoles: '',
      registration: format(new Date(createdAt), 'dd/MM/yyyy'),
      location,
      status: status || ApplicationCandidateStatus.NOT_ENOUGH_INFORMATION,
    };
  });
};

export const selectMetaDataApplications = (state) => state?.applications?.meta;

export const  selectApplicationsStatus = (state) => state?.applications?.loading;