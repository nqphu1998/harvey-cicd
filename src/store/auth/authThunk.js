import { createAsyncThunk } from '@reduxjs/toolkit';
import { errorToObject } from '@/utils/errorUtil';
import { setAuthToken, removeAuthToken } from '@/utils/localStorageUtil';
import { AuthService } from '@/services';

export const login = createAsyncThunk(
  'auth/login',
  async ({ email, password }, thunkAPI) => {
    try {
      const token = await AuthService.login({ email, password });
      setAuthToken(token);
      return token;
    } catch (error) {
      return thunkAPI.rejectWithValue(errorToObject(error));
    }
  },
);

export const logout = createAsyncThunk('auth/logout', async (_, thunkAPI) => {
  try {
    // await AuthService.logout();
    removeAuthToken();
  } catch (error) {
    console.log(error);
  }
});
