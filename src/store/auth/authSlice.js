import { createSlice } from '@reduxjs/toolkit';
import { LoadingStatus } from '@/constants';
import { getAuthToken, removeAuthUser } from '@/utils/localStorageUtil';
import { login } from './authThunk';

const initialState = {
  loading: LoadingStatus.idle,
  error: null,
  token: getAuthToken(),
};

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    clear(state) {
      state.loading = LoadingStatus.idle;
      state.token = null;
      state.error = null;
    },
  },
  extraReducers: {
    [login.pending]: (state) => {
      state.loading = LoadingStatus.pending;
    },
    [login.fulfilled]: (state, { payload: token }) => {
      state.loading = LoadingStatus.fulfilled;
      state.error = null;
      state.token = token;
    },
    [login.rejected]: (state, { payload: error }) => {
      state.loading = LoadingStatus.rejected;
      state.error = error;
      state.token = null;
    },
  },
});

export const { putUser, clear } = slice.actions;
export default slice.reducer;

// Extra Actions

export const logout = () => async (dispatch) => {
  removeAuthUser();
  dispatch(clear());
};
