import { configureStore, combineReducers } from '@reduxjs/toolkit';
import auth from './auth/authSlice';
import applications from './application/applicationSlice';

const reducer = combineReducers({
  auth,
  applications
});

const rootReducer = (state, action) => {
  if (action.type === 'auth/logout/fulfilled') {
    return reducer(undefined, action);
  }
  return reducer(state, action);
};

const store = configureStore({
  reducer: rootReducer,
});

export default store;
